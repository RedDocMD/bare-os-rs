CC := $(TARGET)-gcc
AS := $(TARGET)-as
BUILD_DIR := build
ISODIR := $(BUILD_DIR)/isodir
CFLAGS := -std=gnu89 -ffreestanding -O2 -Wall -Wextra

run: $(BUILD_DIR)/myos.iso
	qemu-system-i386 -cdrom $(BUILD_DIR)/myos.iso

$(BUILD_DIR)/myos.iso: $(BUILD_DIR)/myos.bin grub.cfg
	rm -rf $(ISODIR)
	mkdir -p $(ISODIR)/boot/grub
	cp $(BUILD_DIR)/myos.bin $(ISODIR)/boot
	cp grub.cfg $(ISODIR)/boot/grub
	grub-mkrescue -o $@ $(ISODIR)

$(BUILD_DIR)/myos.bin: $(BUILD_DIR)/boot.o $(BUILD_DIR)/kernel.a
	$(CC) -T linker.ld -o $@ -ffreestanding -O2 -nostdlib $^ -lgcc

$(BUILD_DIR)/boot.o: boot.s
	$(AS) -o $@ $<

$(BUILD_DIR)/kernel.a: src/*.rs
	cargo build --release
	cp target/i686-bare_os/release/libbare_rs.a $(BUILD_DIR)/kernel.a

full-clean: clean
	cargo clean

clean:
	rm -rf $(BUILD_DIR)/*

.PHONY: run clean full-clean