#[repr(u8)]
#[allow(dead_code)]
pub enum VgaColor {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    LightMagenta = 13,
    LigthBrown = 14,
    White = 15
}

pub struct ColorCode(u8);

impl ColorCode {
    pub fn new(fg: VgaColor, bg: VgaColor) -> ColorCode {
        ColorCode((fg as u8) | (bg as u8) << 4)
    }
}

pub const VGA_WIDTH: usize = 80;
pub const VGA_HEIGHT: usize = 25;